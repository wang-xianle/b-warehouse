function ajax(options) {
    let xhr = new XMLHttpRequest()

    // 把对象转成字符串
    let str = queryString(options.data)

    // type默认值
    options.type = options.type || 'GET'

    if (options.type.toUpperCase() === 'GET') {
        xhr.open('GET', options.url + '?' + str, true)
        xhr.send()
    } else {
        xhr.open('POST', options.url, true)
        // 跟后台交互post必须要设置一个请求头！！！
        xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded')
        xhr.send(str)
    }
    

    xhr.onload = function () {
        options.success && options.success(xhr.responseText)
    }
}

function queryString(obj) {
    var arr = []

    for (let attr in obj) {
        arr.push(attr + '=' + obj[attr])
    }

    return arr.join('&')
}